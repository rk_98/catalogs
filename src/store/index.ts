import { types } from 'mobx-state-tree';
import { nanoid } from 'nanoid';

const Categories = types
	.model({
		name: types.string,
		id: types.identifier,
	})
	.actions((self) => {
		return {
			changeName(newName: string) {
				self.name = newName;
			},
		};
	});

const Store = types
	.model({
		categoriesList: types.array(Categories),
	})
	.actions((self) => {
		return {
			createCategory(name: string) {
				self.categoriesList.push({
					name,
					id: nanoid(6),
				});
			},
		};
	});

export default Store;
