import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Store from './store'
import Main from './components/Main'

const store = Store.create()
export const StoreContext = React.createContext(store)

ReactDOM.render(
    <StoreContext.Provider value={store}>
        <Main />
    </StoreContext.Provider>
    , document.getElementById('root'))