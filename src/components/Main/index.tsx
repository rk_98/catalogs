import * as React from 'react';
import { StoreContext } from '../..'

const Main = () => {
    const { categoriesList } = React.useContext(StoreContext)
    console.log(categoriesList)
    return (
        <div>
            <h1>Hello</h1>
        </div>
    )
}

export default Main